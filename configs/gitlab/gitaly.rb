gitaly['enable'] = true
gitlab_rails['auto_migrate'] = false
gitlab_rails['internal_api_url'] = 'https://GITLAB_DOMAIN.SITE_DOMAIN.ROOT_DOMAIN'
gitaly['auth_token'] = 'CI_SECRET_GITLAB_RB_GITALY_AUTH_TOKEN'
gitlab_shell['secret_token'] = 'CI_SECRET_GITLAB_RB_GITALY_AUTH_TOKEN'

gitaly['tls_listen_addr'] = "0.0.0.0:9999"
gitaly['certificate_path'] = "/etc/gitlab/ssl/HOSTNAME.ROOT_DOMAIN.crt"
gitaly['key_path'] = "/etc/gitlab/ssl/HOSTNAME.ROOT_DOMAIN.key"

git_data_dirs({
  'gitalyNODE' => {
    'path' => '/mnt/DO_VOLUME/git-data'
  },
})

user['username'] = "git"
user['group'] = "git"
user['uid'] = 65432
user['gid'] = 65432
user['shell'] = "/bin/sh"
user['home'] = "/var/opt/gitlab"
