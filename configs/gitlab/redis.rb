roles(['redis_master_role'])
redis['enable'] = true
redis['bind'] = '0.0.0.0'
redis['tls_port'] = 6379
redis['tls_cert_file'] = "/etc/gitlab/ssl/HOSTNAME.ROOT_DOMAIN.crt"
redis['tls_key_file'] = "/etc/gitlab/ssl/HOSTNAME.ROOT_DOMAIN.key"
redis['password'] = 'CI_SECRET_GITLAB_RB_REDIS_PASSWORD'
gitlab_rails['auto_migrate'] = false

