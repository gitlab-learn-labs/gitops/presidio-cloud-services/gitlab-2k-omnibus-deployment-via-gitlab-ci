#! /bin/bash
# shellcheck disable=SC1090

set -peu

source "vms/default"
source "${1}"


until [[ -v DO_ID ]]; do
  DO_ID=$(curl -s -X GET -H "Content-Type: application/json" -H "Authorization: Bearer $DO_TOKEN" "https://api.digitalocean.com/v2/droplets?name=${DO_NAME}" | jq .droplets[].id)
  sleep 5
done
sleep 30
source "storage/${DO_VOLUME}"
curl -s -X POST \
      -H "Content-Type: application/json" \
      -H "Authorization: Bearer ${DO_TOKEN}" \
      -d "{\"type\": \"attach\", \"volume_name\": \"${STORAGE_NAME}\", \"region\": \"${STORAGE_REGION}\", \"droplet_id\": \"${DO_ID}\",\"tags\":[\"${STORAGE_LABEL}\"] }" \
      "https://api.digitalocean.com/v2/volumes/actions"

shutdown -r +1
