#! /bin/bash
set -epu

if [[ -e /etc/gitlab/gitlab-secrets.json ]]; then
  curl -s --request POST --header "PRIVATE-TOKEN:GL_TOKEN" "https://git.ROOT_DOMAIN/api/v4/projects/112/variables" --form "value=$(cat /etc/gitlab/gitlab-secrets.json)" --form "variable_type=file" --form "key=SECRETS_BRANCH_JSON" >/dev/null
  curl -s --request PUT --header "PRIVATE-TOKEN:GL_TOKEN" "https://git.ROOT_DOMAIN/api/v4/projects/112/variables/SECRETS_BRANCH_JSON" --form "value=$(cat /etc/gitlab/gitlab-secrets.json)" --form "variable_type=file" >/dev/null
fi

declare -A RAILS_SERVICES=

RAILS_SERVICES["GITLAB"]="GITLAB_DOMAIN"
RAILS_SERVICES["PAGES"]="PAGES_DOMAIN"
RAILS_SERVICES["REGISTRY"]="REGISTRY_DOMAIN"

# shellcheck disable=SC2048
for SERVICE in ${RAILS_SERVICES[*]}; do
  if [[ "${HOSTNAME}" =~ 'rails' ]] && [[ -s "/etc/gitlab/ssl/${SERVICE}.SITE_DOMAIN.ROOT_DOMAIN.crt" ]]; then
      set +e
      CRTMOD="$(openssl x509 -noout -modulus -in "/etc/gitlab/ssl/${SERVICE}.SITE_DOMAIN.ROOT_DOMAIN.crt" | grep -P -o '(?<=Modulus=).*')"
      KEYMOD="$(openssl rsa -check -noout -modulus -in "/etc/gitlab/ssl/${SERVICE}.SITE_DOMAIN.ROOT_DOMAIN.key" | grep -P -o '(?<=Modulus=).*')"
      set -e

      if [[ "${CRTMOD}" = "${KEYMOD}" ]]; then
          curl -s --request POST --header "PRIVATE-TOKEN:GL_TOKEN" "https://git.ROOT_DOMAIN/api/v4/projects/112/variables" --form "value=$(cat "/etc/gitlab/ssl/${SERVICE}.SITE_DOMAIN.ROOT_DOMAIN.crt")" --form "variable_type=file" --form "key=LE_BRANCH_CRT" >/dev/null
          curl -s --request POST --header "PRIVATE-TOKEN:GL_TOKEN" "https://git.ROOT_DOMAIN/api/v4/projects/112/variables" --form "value=$(cat "/etc/gitlab/ssl/${SERVICE}.SITE_DOMAIN.ROOT_DOMAIN.key")" --form "variable_type=file" --form "key=LE_BRANCH_KEY" >/dev/null
          curl -s --request PUT --header "PRIVATE-TOKEN:GL_TOKEN" "https://git.ROOT_DOMAIN/api/v4/projects/112/variables/LE_BRANCH_CRT" --form "value=$(cat "/etc/gitlab/ssl/${SERVICE}.SITE_DOMAIN.ROOT_DOMAIN.crt")" --form "variable_type=file" >/dev/null
          curl -s --request PUT --header "PRIVATE-TOKEN:GL_TOKEN" "https://git.ROOT_DOMAIN/api/v4/projects/112/variables/LE_BRANCH_KEY" --form "value=$(cat "/etc/gitlab/ssl/${SERVICE}.SITE_DOMAIN.ROOT_DOMAIN.key")" --form "variable_type=file" >/dev/null
      fi
  fi
done
