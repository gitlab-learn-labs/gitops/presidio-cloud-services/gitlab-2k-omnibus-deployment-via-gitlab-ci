#! /bin/bash
# shellcheck disable=SC1090

set -peu

source "vms/default"
source "${1}"

if [[ -v DO_VOLUME ]]; then
  source "storage/${DO_VOLUME}"
  if [[ $(curl -s -X GET -H "Content-Type: application/json" -H "Authorization: Bearer $DO_TOKEN" "https://api.digitalocean.com/v2/volumes?name=$STORAGE_NAME" | jq .meta.total) -eq 0 ]]; then
    curl -s -X POST \
    -H "Content-Type: application/json" \
    -H "Authorization: Bearer $DO_TOKEN" \
    -d "{\"size_gigabytes\":${STORAGE_SIZE}, \"name\": \"${STORAGE_NAME}\", \"description\": \"${STORAGE_DESCRIPT}\", \"region\": \"${STORAGE_REGION}\", \"filesystem_type\": \"${STORAGE_FSTYPE}\", \"filesystem_label\": \"${STORAGE_LABEL}\"}" \
    "https://api.digitalocean.com/v2/volumes"
  fi
fi
