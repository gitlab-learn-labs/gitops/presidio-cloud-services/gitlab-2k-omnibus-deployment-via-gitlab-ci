#! /bin/bash
# shellcheck disable=SC1090

set -peu

source "vms/default"
source "${1}"

if [[ -v DO_NAME ]]; then
  if [[ "${HOSTNAME}" == "${DO_NAME}" ]]; then 
    exit 0
  else
    DO_ID=$(curl -s -X GET -H "Content-Type: application/json" -H "Authorization: Bearer $DO_TOKEN" "https://api.digitalocean.com/v2/droplets?name=${DO_NAME}" | jq .droplets[].id)

    if  [[ -v DO_ID ]] && [[ -v DO_VOLUME ]]; then
        source "storage/${DO_VOLUME}"
        echo "Detaching volumes from VMs before deleting VMs."
        curl -s -X POST \
                -H "Content-Type: application/json" \
                -H "Authorization: Bearer ${DO_TOKEN}" \
                -d "{\"type\": \"detach\", \"volume_name\": \"${STORAGE_NAME}\", \"region\": \"${STORAGE_REGION}\", \"droplet_id\": \"${DO_ID}\",\"tags\":[\"${STORAGE_LABEL}\"] }" \
                "https://api.digitalocean.com/v2/volumes/actions"
    fi

    sleep 30 
    echo "Deleting $DO_NAME."
    curl -s -X DELETE \
            -H "Content-Type:\ application/json" \
            -H "Authorization:\ Bearer $DO_TOKEN" \
            "https://api.digitalocean.com/v2/droplets/${DO_ID}"
  fi
else
  exit 0
fi

