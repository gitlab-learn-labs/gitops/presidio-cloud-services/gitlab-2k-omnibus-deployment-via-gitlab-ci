#! /bin/bash
set -epu

while getopts "k:K:c:C:" FILE; do
  case ${FILE} in
    k) INPUT_KEY="${OPTARG}";;
    K) OUTPUT_KEY="${OPTARG}";;
    c) INPUT_CERT="${OPTARG}";;
    C) OUTPUT_CERT="${OPTARG}";;
    *)
      printf "Invalid option.\n"
       exit 1;;
  esac
done

if [[ -v INPUT_KEY ]] && [[ -v OUTPUT_KEY ]] && [[ -v INPUT_CERT ]] && [[ -v OUTPUT_CERT ]]; then
  sudo install -T -m 644 -o root -g root "${INPUT_CERT}" "${OUTPUT_CERT}" && sudo test -e "${OUTPUT_CERT}"
  sudo install -T -m 644 -o root -g root "${INPUT_KEY}" "${OUTPUT_KEY}" && sudo test -e "${OUTPUT_KEY}"
fi
