#! /bin/bash
# shellcheck disable=SC1090

set -epu

source "${1}"
 
if [[ -v DO_NAME ]]; then 
  DO_ID=$(curl -s -X GET -H "Content-Type:\ application/json" -H "Authorization:\ Bearer $DO_TOKEN" "https://api.digitalocean.com/v2/droplets?name=${DO_NAME}" | jq .droplets[].id)
  curl -s -X POST -H "Content-Type:\ application/json" -H "Authorization:\ Bearer $DO_TOKEN" -d "{\"type\":\"${2}\"}" "https://api.digitalocean.com/v2/droplets/${DO_ID}/actions"
fi
